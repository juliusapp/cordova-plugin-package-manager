### Cordova Plugin for checking the presence of installed packages in a device

Supported platforms:

 + Android


####Install

```
> cordova plugins add com.juliusapp.pkgmanager
```

###Usage

```javascript
packageManager.isInstalled('com.whatsapp', 
	function(isInstalled){ 
		console.log(isInstalled)
	}, function(error){ 
		console.log(error)
	});
```

var argscheck = require('cordova/argscheck'),
    exec = require('cordova/exec');

var packageManager = {
	isInstalled:function(packageName, successCB, failureCB) {
    	argscheck.checkArgs('sfF', 'PkgManager.isInstalled', arguments);
    	exec(successCB, failureCB, 'PkgManager', 'isInstalled', [packageName]);
	},
	launch:function(packageName, openUri, successCB, failureCB) {
    	//TODO: check null's ara passable as string?
    	argscheck.checkArgs('ssfF', 'PkgManager.launch', arguments);
    	exec(successCB, failureCB, 'PkgManager', 'launch', [packageName, openUri]);
	},
	install:function(packageName, installUri, successCB, failureCB) {
    	argscheck.checkArgs('ssfF', 'PkgManager.install', arguments);
    	exec(successCB, failureCB, 'PkgManager', 'install', [packageName, installUri]);
	},
	installAndClaim:function(packageName, rewardId, authToken, installUri, successCB, failureCB) {
    	argscheck.checkArgs('ssssfF', 'PkgManager.installAndClaim', arguments);
    	exec(successCB, failureCB, 'PkgManager', 'installAndClaim', [packageName, rewardId, authToken, installUri]);
	},
	getInstalledApps:function(successCB, failureCB) {
		argscheck.checkArgs('fF', 'PkgManager.getInstalledApps', arguments);
		exec(successCB, failureCB, 'PkgManager', 'getInstalledApps', []);
	},
	getSharedPreferences: function(successCB, failureCB) {
		argscheck.checkArgs('fF', 'PkgManager.getSharedPreferences', arguments);
		exec(successCB, failureCB, 'PkgManager', 'getSharedPreferences', []);
	}
};

module.exports = packageManager;
/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.juliusapp.pkgmanager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * This class exposes methods in Cordova that can be called from JavaScript to
 * handle the application package installations.
 * 
 * <p>
 * Note:
 * 
 * Threading: JavaScript in the WebView does not run on the UI thread. It runs
 * on the WebCore thread. The execute method also runs on the WebCore thread.
 * 
 * To interact with the UI, this class uses the following:
 * 
 * cordova.getActivity().runOnUiThread(new Runnable() { public void run() {...
 * callbackContext.success(); // Thread-safe. }});
 * 
 * </p>
 * 
 * @author sergio
 * 
 */
public class PkgManager extends CordovaPlugin {

	public static final String ACTION_CHECK_PKG_INSTALLED = "isInstalled";

	public static final String ACTION_LAUNCH = "launch";

	public static final String ACTION_INSTALL = "install";

	public static final String ACTION_INSTALL_AND_CLAIM = "installAndClaim";

	public static final String ACTION_GET_INSTALLED_APPS = "getInstalledApps";

	public static final String ACTION_GET_SHARED_PREFERENCES = "getSharedPreferences";
	
	public static final String LOG_TAG = "PkgManagerPlugin";

	private static final String PLAY_STORE_URI = "http://play.google.com/store/apps/details?id=";
	
	private static final String NULL = "NULL";
	
	private static Context context = null;

	private static PackageManager manager = null;

	private static Map<String, CallbackContext> callbacksMap = new HashMap<String, CallbackContext>();

	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		context = cordova.getActivity().getApplicationContext();
	}

	@Override
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		Log.d(LOG_TAG, "Action is " + action);
		if (ACTION_CHECK_PKG_INSTALLED.equals(action)) {
			return checkPkgInstalled(args, callbackContext);
		} else if (ACTION_INSTALL_AND_CLAIM.equals(action)) {
			return installAndClaim(args, callbackContext);
		} else if (ACTION_INSTALL.equals(action)) {
			return install(args, callbackContext);
		} else if (ACTION_LAUNCH.equals(action)) {
			return launch(args, callbackContext);
		} else if (ACTION_GET_INSTALLED_APPS.equals(action)) {
			return getInstalledApps(callbackContext);
		} else if (ACTION_GET_SHARED_PREFERENCES.equals(action)) {
			return getSharedPreferences(callbackContext);
		} else {
			callbackContext.error(String.format("Invalid action passed: %s",
					action));
			return false;
		}
	}

	
	public static CallbackContext getCallbackContext(String key) {
		return callbacksMap.get(key);
	}

	public static boolean deleteCallbackContext(String key) {
		callbacksMap.put(key, null);
		return true;
	}

	public static String getPreference(Context context, String key) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(key, null);
	}

	public static void setPreference(Context context, String key, String value) {
		PreferenceManager.getDefaultSharedPreferences(context).edit()
				.putString(key, value).commit();
	}

	public static void deletePreference(String key) {
		PreferenceManager.getDefaultSharedPreferences(context).edit()
				.remove(key).commit();
	}

	// --------------------------------------------------------------------------
	// LOCAL METHODS
	// --------------------------------------------------------------------------

	private boolean getInstalledApps(final CallbackContext callbackContext) {
		// get a list of installed apps.
		cordova.getThreadPool().execute(new Runnable() {
			@Override
			public void run() {
				List<ApplicationInfo> packages = getInstalledApps();
				JSONArray arr = new JSONArray();
				for (ApplicationInfo packageInfo : packages) {
					arr.put(packageInfo.packageName);
				}
				Log.d(LOG_TAG, "JsonArray info list is " + arr);
				callbackContext.success(arr);
			}
		});

		return true;
	}

	private boolean launch(JSONArray args, CallbackContext callbackContext)
			throws JSONException {
		String packageName;
		packageName = args.getString(0);
		String openUri = args.getString(1);
		Log.d(LOG_TAG, "openUri is : " + openUri);
		boolean isEqual = openUri.equalsIgnoreCase(NULL);
		Log.d(LOG_TAG, "openUri isEqual to NULL?? : " + isEqual);

		if (!openUri.equalsIgnoreCase(NULL)) {
			openUri(openUri);
			callbackContext.success("Opened uri " + openUri);
		} else {
			launchPackage(packageName);
			callbackContext.success("Launched " + packageName);
		}
		return true;
	}

	private boolean install(JSONArray args, CallbackContext callbackContext)
			throws JSONException {
		String packageName;
		packageName = args.getString(0);
		String installUri = args.getString(1);
		install(installUri, packageName);
		callbackContext.success("Installed package " + packageName);
		return true;
	}

	private boolean getSharedPreferences(CallbackContext callbackContext) throws JSONException {
		Map<String, ?> prefs = PreferenceManager.getDefaultSharedPreferences(context).getAll();
		JSONObject obj = new JSONObject();
		for(String key: prefs.keySet()) {
			obj.put(key, prefs.get(key));
		}
		callbackContext.success(obj);
		return true;
	}

	private boolean installAndClaim(JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		String packageName;
		packageName = args.getString(0);
		String rewardId = args.getString(1);
		String token = args.getString(2);
		String installUri = args.getString(3);

		// Save command callbackContext
		callbacksMap.put(packageName, callbackContext);
		launch(packageName, rewardId, token, installUri, callbackContext);

		PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
		result.setKeepCallback(true);
		callbackContext.sendPluginResult(result);
		return true;
	}

	private boolean checkPkgInstalled(JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		String packageName;
		packageName = args.getString(0);
		boolean installed = appIsInstalled(packageName);
		JSONObject obj = new JSONObject();
		obj.put("installed", installed);
		callbackContext.success(obj);
		return true;
	}

	private List<ApplicationInfo> getInstalledApps() {
		manager = context.getPackageManager();
		return manager.getInstalledApplications(PackageManager.GET_META_DATA);
	}

	private boolean appIsInstalled(String uri) {
		manager = context.getPackageManager();
		boolean app_installed = false;
		try {
			manager.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	private void launchPackage(final String appPackageName) {
		cordova.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Log.d(LOG_TAG, "Launching package: " + appPackageName);
				Intent i = manager.getLaunchIntentForPackage(appPackageName);
				i.addCategory(Intent.CATEGORY_LAUNCHER);
				context.startActivity(i);
			}
		});
	}

	private void openUri(final String openUri) {
		cordova.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Log.d(LOG_TAG, "Opening app with openingUri: " + openUri);
				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(openUri));
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(i);
			}
		});
	}

	private void install(final String installUri, final String appPackageName) {
		cordova.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				try {
					Log.d(LOG_TAG, "Installing app with installUri: " + installUri);
					Intent i = new Intent(Intent.ACTION_VIEW, Uri
							.parse(installUri));
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(i);
				} catch (android.content.ActivityNotFoundException anfe) {
					Uri uri = Uri.parse(PLAY_STORE_URI + appPackageName);
					Log.d(LOG_TAG, "Installing app with installUri: " + uri.toString());
					Intent i = new Intent(Intent.ACTION_VIEW, uri);
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(i);
				}
			}
		});
	}

	private void launch(final String appPackageName, final String rewardId,
			final String authToken, final String installUri,
			final CallbackContext callbackContext) {
		cordova.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				try {
					Intent i = new Intent(Intent.ACTION_VIEW, Uri
							.parse(installUri));
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					Log.d(LOG_TAG, "Calling intent with rewardId: " + rewardId);
					setPreference(context, appPackageName, rewardId);
					setPreference(context, "julius-token", authToken);
					context.startActivity(i);
				} catch (android.content.ActivityNotFoundException anfe) {
					Log.d(LOG_TAG, "android.content.ActivityNotFoundException "
							+ anfe.getMessage());
					Intent i = new Intent(Intent.ACTION_VIEW, Uri
							.parse(PLAY_STORE_URI + appPackageName));
					i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					setPreference(context, appPackageName, rewardId);
					setPreference(context, "julius-token", authToken);
					context.startActivity(i);
				}
			}
		});
	}

}

package com.juliusapp.pkgmanager;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;

import android.net.Uri;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Receiver extends BroadcastReceiver {

	private static final String JULIUS_TOKEN = "julius-token";

	private static final String UTF_8 = "utf-8";

	private static final String HTTP_STATUS = "HTTP_STATUS";

	private static final String MESSAGE = "message";

	@Override
	public void onReceive(final Context context, Intent intent) {

		final String authToken = PkgManager.getPreference(context, JULIUS_TOKEN);
		final String packageName = intent.getData().getSchemeSpecificPart();
		Log.d(PkgManager.LOG_TAG, "Installed " + packageName);

		AsyncHttpPost asyncHttpPost = newAsyncHttpPost(context, packageName, authToken);

		// Evaluate if rewardId not exist in preferences
		final String rewardId = PkgManager.getPreference(context, packageName);
		if (rewardId != null) {
			Log.d(PkgManager.LOG_TAG, String.format("Claiming reward %s...", rewardId));
			asyncHttpPost.execute(rewardId + "/claims");
		} else {
			Log.d(PkgManager.LOG_TAG, String.format("Reward already claimed for package %s", packageName));
		}

	}

	private AsyncHttpPost newAsyncHttpPost(final Context context, final String packageName, final String authToken) {
		return new AsyncHttpPost(authToken) {
			@Override
			protected void onPostExecute(HttpResponse response) {
				StatusLine statusLine = response.getStatusLine();
				Log.d(PkgManager.LOG_TAG, "POST status code... " + statusLine.getStatusCode());

				CallbackContext callbackContext = PkgManager.getCallbackContext(packageName);
				PkgManager.deleteCallbackContext(packageName);

				PluginResult pluginResult;
				try {
					pluginResult = generateResult(response, statusLine);
				} catch (JSONException e) {
					pluginResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
				}
				pluginResult.setKeepCallback(false);
				Log.d(PkgManager.LOG_TAG, "Sending PluginResult...");
				callbackContext.sendPluginResult(pluginResult);
				callbackContext = null;
				try {
					if (pluginResult.getStatus() == PluginResult.Status.OK.ordinal()) {
						JSONObject json = new JSONObject(pluginResult.getMessage());
						JSONObject openUriJSONObject = json.optJSONObject("openUri");
						String openUri = openUriJSONObject != null ? openUriJSONObject.getString("uri") : "";
						if (openUri.length() > 0) {
							Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(openUri));
							i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							context.startActivity(i);
						}
					}
				} catch (JSONException e) {
					Log.e(PkgManager.LOG_TAG, e.getMessage());
				}
			}

			private PluginResult generateResult(HttpResponse response, StatusLine statusLine) throws JSONException {
				JSONObject message;
				PluginResult.Status status;
				if (statusLine.getStatusCode() == HttpURLConnection.HTTP_CREATED) {
					try {
						message = new JSONObject(new String(EntityUtils.toByteArray(response.getEntity()), UTF_8));
						status = PluginResult.Status.OK;
					} catch (UnsupportedEncodingException e) {
						Log.e(PkgManager.LOG_TAG, e.getMessage());
						status = PluginResult.Status.ERROR;
						message = newMessage(statusLine, e.getMessage());
					} catch (IOException e) {
						Log.e(PkgManager.LOG_TAG, e.getMessage());
						status = PluginResult.Status.ERROR;
						message = newMessage(statusLine, e.getMessage());
					}

					// Delete package from preferences
					PkgManager.deletePreference(packageName);

				} else if (statusLine.getStatusCode() >= HttpURLConnection.HTTP_BAD_REQUEST) {
					Log.e(PkgManager.LOG_TAG, statusLine.getReasonPhrase());
					status = PluginResult.Status.OK;
					message = newMessage(statusLine, "Bad return status");
				} else {
					status = PluginResult.Status.ERROR;
					message = newMessage(statusLine, "Error return status");
				}
				return new PluginResult(status, message);
			}

			private JSONObject newMessage(StatusLine statusLine, String message) throws JSONException {
				final JSONObject data = new JSONObject();
				data.put(MESSAGE, message);
				data.put(HTTP_STATUS, statusLine.getStatusCode());

				return data;
			}
		};
	}
}

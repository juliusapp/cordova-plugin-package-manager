package com.juliusapp.pkgmanager;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.os.AsyncTask;
import android.util.Log;

public class AsyncHttpPost extends AsyncTask<String, String, HttpResponse> {
	
	private static final String UTF_8 = "UTF-8";

	// Source from
	// http://stackoverflow.com/questions/7860538/android-http-post-asynctask
	private Map<String, String> mData = null;

	private String authHeader = null;

	public AsyncHttpPost(String authorization) {
		this(authorization, new HashMap<String, String>());
	}

	public AsyncHttpPost(String authorization, Map<String, String> data) {
		authHeader = authorization;
		if (mData == null) {
			mData = new HashMap<String, String>();
		}
	}

	@Override
	protected HttpResponse doInBackground(String... params) {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(params[0]);// in this case, params[0] is
												// URL
		post.addHeader("Authorization", authHeader);
		Log.d(PkgManager.LOG_TAG, "Performing POST in background... "
				+ params[0]);
		ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
		Iterator<String> it = mData.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			nameValuePair.add(new BasicNameValuePair(key, mData.get(key)));
		}

		try {
			post.setEntity(new UrlEncodedFormEntity(nameValuePair, UTF_8));
		} catch (UnsupportedEncodingException e1) {
			Log.e(PkgManager.LOG_TAG, e1.getMessage());
		}
		HttpResponse response = null;
		try {
			response = client.execute(post);
		} catch (ClientProtocolException e) {
			Log.e(PkgManager.LOG_TAG, e.getMessage());
		} catch (IOException e) {
			Log.e(PkgManager.LOG_TAG, e.getMessage());
		}
		return response;
	}

}